package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Usability unit tests")
public class appSpec {

    @Test
    public void EmptyCheck() {
        assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            Amount amount = new Amount("");
        });
    }

    @Test
    public void NullCheck() {
        assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            Amount amount = new Amount(null);
        });
    }

    @Test
    public void BlankCheck() {
        assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            Amount amount = new Amount("       ");
        });
    }

    @Test
    public void ThousandsNeedsApproval() {
        Main app = new Main();
        Amount amount = new Amount("1500");
        boolean res = app.approval(amount);
        assertTrue(res, () -> "1500 needs approval");
    }

    @Test
    public void FileHundredsDoesNotNeedApproval() {
        Main app = new Main();

        Amount amount = new Amount("500");
        boolean res = app.approval(amount);
        assertFalse(res, () -> "500 does not need approval");
    }

}
