package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {
        assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            Amount amount = new Amount("2147483641111");
            boolean res = app.approval(amount);
            assertTrue(res,() -> "Bigger than int max size needs approval");
        });
    }

    @Test
    public void LessThanIntMinSizeNeedsApproval() {
        assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            Amount amount = new Amount("-2147483648999");
            boolean res = app.approval(amount);
            assertTrue(res,() -> "Less than int min size needs approval");
        });
    }
}
