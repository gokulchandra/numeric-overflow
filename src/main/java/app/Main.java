package app;

public class Main {

    public static void main(String[] args) {

        Main app = new Main();

        Amount amount = new Amount("999");
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = new Amount("2000");
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(Amount amount) {
        return amount.requiresApproval();
    }


}
